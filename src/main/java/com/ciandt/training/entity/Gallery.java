package com.ciandt.training.entity;

import java.util.List;

public class Gallery<T> {

        private int id;
        private List<T> images;

        public Gallery() {
        }

        public Gallery(int galleryId) {
            this.id = galleryId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<T> getImages() {
            return images;
        }

        public void setImages(List<T> images) {
            this.images = images;
        }
}
